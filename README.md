# Common library

Version are idenfified with semver tagging, eg: 0.0.1

## Usage in composer

```json
{
    "require": {
        "nutellino/common" : "0.0.1"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://gitlab.com/nutellino-playground/lemp-demo/library/common.git"
        }
    ]
}
```
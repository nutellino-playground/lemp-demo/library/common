<?php


namespace Nutellino\Common;

use \PDO as PDO;

class Example
{
    private $conn;
    private $tableName = "example";

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getAll()
    {

        $returnArray = [];

        $query = "SELECT *
                  FROM
                  " . $this->tableName ." order by id asc ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $returnArray[] = $row;
        }
        return $returnArray;

    }


}
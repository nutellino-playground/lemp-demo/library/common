<?php

namespace Nutellino\Common;
use \PDO as PDO;
use \PDOException as PDOException;

class Database
{
    public $conn;
    // Connect to database
    public function getConnection()
    {
        $this->conn = null;
        try
        {
            $this->conn = new PDO("mysql:host=" . getenv("MYSQL_HOST") . ";dbname=" . getenv("MYSQL_DATABASE"), getenv("MYSQL_USER"), getenv("MYSQL_PASSWORD"));
            $this->conn->exec("set names utf8");
        }
        catch(PDOException $exception)
        {
            echo "Connection error! : " . $exception->getMessage();
        }
        return $this->conn;
    }

}